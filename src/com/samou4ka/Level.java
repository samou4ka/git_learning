package com.samou4ka;

public interface Level {
    int computerLevel(String s, char symbol);
}
