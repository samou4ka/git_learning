package com.samou4ka;

import java.util.Random;

public class EasyLevel implements Level {

    @Override
    public int computerLevel(String s, char symbol){
        int move;
        Random random = new Random();
        while (true) {
            move = random.nextInt(9) + 1;
            if (s.charAt(move) == ' ') {
                return move;
            }
        }
    }
}
