package com.samou4ka;

public class Move{
    private int index;
    private int score;

    public int getIndex() {
        return index;
    }

    public int getScore() {
        return score;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
