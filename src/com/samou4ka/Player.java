package com.samou4ka;

public class Player{
    String name;

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    Level level;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
